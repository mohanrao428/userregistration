package com.user.registration.service;

import com.user.registration.dto.RegistrationDTO;

public interface IRegistrationService {

    public void sendUserRegistrationDetails(RegistrationDTO registrationDTO);
}
