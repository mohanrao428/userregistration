package com.user.registration.service.impl;


import com.user.registration.configuration.RabbitMqRegistrationProperties;
import com.user.registration.dto.RegistrationDTO;
import com.user.registration.service.IRegistrationService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistrationServiceImpl implements IRegistrationService {

    @Autowired
    RabbitMqRegistrationProperties rabbitMQProperties;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public void sendUserRegistrationDetails(RegistrationDTO registrationDTO) {
        rabbitTemplate.convertAndSend(rabbitMQProperties.getExchangeName(), rabbitMQProperties.getRoutingKey(), registrationDTO);
    }
}
