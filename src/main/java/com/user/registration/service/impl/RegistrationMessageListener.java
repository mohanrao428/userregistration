package com.user.registration.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.user.registration.dto.RegistrationDTO;
import com.user.registration.util.RegistrationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class RegistrationMessageListener {
    private static final Logger logger = LoggerFactory.getLogger(RegistrationMessageListener.class);

    String messageToLog = "User %s with phone %s has just signed up!";

    /**
     * @param message
     * This method will handle the messages received from message queue asynchronously by using Spring ThreadPoolTaskExecutor
     * @throws JsonProcessingException
     */
    @Async
    public void listen(byte[] message) throws JsonProcessingException {
        String msg = new String(message);
        RegistrationDTO registrationDTO = new ObjectMapper().readValue(msg, RegistrationDTO.class);

        String phoneNumberInFrenchNumberFormat = RegistrationUtil.getPhoneNumberInFrenchNumberFormat(registrationDTO.getPhone());
        String firstAndLastName = RegistrationUtil.getFirstAndLastName(registrationDTO.getFullName());
        String formattedMessage = String.format(messageToLog, firstAndLastName, phoneNumberInFrenchNumberFormat);

        logger.info(formattedMessage);
    }
}
