package com.user.registration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@SpringBootApplication
@EnableAsync
public class RegistrationApplication {

    @Value("${consumer.threadpool.maxthreads}")
    private int maxPoolSize;

    @Value("${consumer.threadpool.minthreads}")
    private int minPoolSize;

    @Value("${consumer.threadpool.queueCapacity}")
    private int queueCapacity;

    public static void main(String[] args) {
        SpringApplication.run(RegistrationApplication.class, args);
    }


    /**
     * This will define an executorService to hanlde the @Async tasks
     */
    @Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(minPoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.setThreadNamePrefix("MessageProcess-");
        executor.initialize();
        return executor;
    }
}
