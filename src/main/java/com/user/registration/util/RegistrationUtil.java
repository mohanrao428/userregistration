package com.user.registration.util;

public class RegistrationUtil {

    private static final String FRENCH_NUMBER_PREFIX = "+33";

    public static String getFirstAndLastName(String fullName) {
        String[] names = fullName.split(" ");

        if (names.length == 1) {
            return fullName;
        } else {
            return names[0] + " " + names[names.length - 1];
        }
    }

    public static String getPhoneNumberInFrenchNumberFormat(String phoneNumber) {
        phoneNumber = phoneNumber.replaceAll("\\s", "");

        if (!phoneNumber.startsWith(FRENCH_NUMBER_PREFIX)) {
            phoneNumber = FRENCH_NUMBER_PREFIX + phoneNumber;
        }

        return phoneNumber;
    }
}
