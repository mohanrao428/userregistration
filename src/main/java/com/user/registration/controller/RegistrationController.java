package com.user.registration.controller;

import com.user.registration.dto.RegistrationDTO;
import com.user.registration.service.IRegistrationService;
import com.user.registration.validator.RegistrationRequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegistrationController {

    @Autowired
    private IRegistrationService messageProducerService;

    @Autowired
    private RegistrationRequestValidator registrationRequestValidator;


    @PostMapping(
            value = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity registration(@RequestBody RegistrationDTO registrationDTO) {
        if (registrationRequestValidator.isRequestValid(registrationDTO)) {
            messageProducerService.sendUserRegistrationDetails(registrationDTO);

            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().build();
        }

    }

}

