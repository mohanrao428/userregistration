package com.user.registration.validator;

import com.user.registration.dto.RegistrationDTO;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class RegistrationRequestValidator {

    public boolean isRequestValid(RegistrationDTO registrationDTO) {
        if (StringUtils.isEmpty(registrationDTO.getFullName()) || StringUtils.isEmpty(registrationDTO.getPhone())) {
            return false;
        }
        return true;
    }
}
