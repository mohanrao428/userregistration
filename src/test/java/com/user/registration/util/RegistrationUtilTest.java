package com.user.registration.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RegistrationUtilTest {

    @Test
    void testGetFirstAndLastNameWhereNameHavingMiddleName() {
        String name = "Mohan Rao Dugyala";
        String firstAndLastName = RegistrationUtil.getFirstAndLastName(name);
        assertEquals("Mohan Dugyala", firstAndLastName);
    }

    @Test
    void testGetFirstAndLastNameWhereNameWithoutMiddleName() {
        String name = "Mohan Dugyala";
        String firstAndLastName = RegistrationUtil.getFirstAndLastName(name);
        assertEquals("Mohan Dugyala", firstAndLastName);
    }

    @Test
    void getPhoneNumberInFrenchNumberFormatWhereNumberHavingSpaces() {
        String number = "98 98 987700";
        String phoneNumberInFrenchNumberFormat = RegistrationUtil.getPhoneNumberInFrenchNumberFormat(number);
        assertEquals("+339898987700", phoneNumberInFrenchNumberFormat);
    }

    @Test
    void getPhoneNumberInFrenchNumberFormatWhereNumberAlreadyFormatted() {
        String number = "+3398 98 987700";
        String phoneNumberInFrenchNumberFormat = RegistrationUtil.getPhoneNumberInFrenchNumberFormat(number);
        assertEquals("+339898987700", phoneNumberInFrenchNumberFormat);
    }
}