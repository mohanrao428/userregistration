# User Registration App

### Service available
*post user details service - [http://localhost:8080](http://localhost:8080)

data required in post service :

{
  "fullName": "Mohan Rao Dugyala",
  "phone": "9898 989808"
}


Number of consumer threads that can process
the messages can be controlled by using below properties, default values are 3 and 6 respectively

-consumer.threadpool.minthreads
-consumer.threadpool.maxthreads
